import csv


#Class uses a dictionary to determine if a packet meets any rule that was specified in a CSV file
class Firewall:
	def __init__(self, FileName):
		networkRules = map()

		#Reading CSV file. Method from pythonprogramming.net
		with open(FileName) as cvsfile:
			legible = cvs.reader(cvsfile, delimiter=',')
			for row in legible:
				portHasRange = true
				IPHasRange = true
				direction = row[0]
				protocol = row[1]
				#Turn port into a list containing strings of the upper and lower bounds of the port
				port = row[2].split('-')
				#Make the IP Address into just a number that is a string. No periods.
				ipAddress = row[3].replace(".", "")
				#Turn IP Address into a list containing strings of the upper and lower bounds of the port
				ipAddress = ipAddress.split('-')

				#If the lists of port and ipAddress only contain one element, turn that element into an integer
				if len(port) == 1:
					port = int(port[0])
					portHasRange = false
				if len(ipAddress) == 1:
					ipAddress = int(ipAddress[0])
					IPHasRange = false.
				#There are four possibilities for a rule input which are handled below in which all 
				#possible rule combinations are placed in a dictionary:
				#1. port and IP Address are both single values and not ranges
				if (not portHasRange) and (not IPHasRange):
					 rule = Rule(direction, protocol, port, ipAddress)
					 networkRules.put(rule, 1)
				#2. Port has a Range but IP Does not; 
				if portHasRange and (not IPHasRange):
					minPort = int(port[0])
					maxPort = int(port[1])
					while minPort <= maxPort:
						rule = Rule(direction, protocol, minPort, ipAddress)
					 	networkRules.put(rule, 1)
					 	minPort = minPort + 1
				#3 Port does not have a range, but IP does
				if (not portHasRange) and (IPHasRange):
					minIP = int(ipAddress[0])
					maxIP = int(ipAddress[1])
					while minIP <= maxIP:
						rule = Rule(direction, protocol, port, minIP)
						networkRules.put(rule, 1)
				# Port and IP both have ranges
				if portHasRange and IPHasRange:
					minPort = int(port[0])
					maxPort = int(port[1])
					minIP = int(ipAddress[0])
					minIP = int(ipAddress[1])
					for currPort in range(minPort, maxPort + 1):
						for currIP in range(minIP, maxIP + 1):
							rule = Rule(direction, protocol, currPort, currIP)
							networkRules.put(rule, 1)

		#Retuns the hash of the hashcodes of all inputs to be used as a key for the dictionary
	def Rule(direction, protocol, port, ipAddress):
		toHash = hash(direction) + hash(protocol) + hash(port) + hash(ipAddress)
		key = hash(tohash)
		return key
		#Returns whether the givern input meets a rule by checking its existence in the dictionary
	def accept_packet(direction, protocol, port, ipAddress):
		attempt = Rule(direction, protocol, port, ipAddress)
		if networkRules.get(attempt) == 1:
			return True
		return False

















