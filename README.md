1. Functionality:
	I was not able to test my code during the allotted time. I created a CSV File 
	using Excel but I ran out of time before testing my implementation.
	However, I am certain that the idea behind my code is a valid solution to the problem.

2. Design:
	My intial thought was to create a list of several Rule objects and iterate through them
	until the given packet matched valid rule. However, this would be time consuming. At worst
	case, we would have to iterate through the entire list of rules only to find out the packet
	is invalid. I would have to do this for every packet whose validity we need to check.
	My code creates unique Rule objects which return a key to be used in in a dictionary
	in order to assert the existence of a valid rule. The key in the Rule object is
	determined by hashing the result of the sum of the input hashcodes, a method I had
	used in a previous project. The most time consuming part of the solution would be
	the inputting of all valid rules into the dictionary. When it comes to ranges of ports
	and IP Addresses, I had to worry about including every possible combination that was
	considered valid. However, to check if a packet is valid only takes constant time.

3. Future Refinements and Optimizations:
	If I had more time to optimize this problem and think about other possible solutions,
	I would have tried to figure out a different implentation which could ignore one of the
	given packet parameters, such as IP Addresses. In my implementation I have to worry about
	inputting every possible valid rule into the dictionary,which could potentially have a
	bad runtime. If I could eliminate one of the parameters, there would be fewer possible 
	combinations of valid rules and the time to input these rules into the dictionary would greatly
	decrease.
4. Anything Else You'd Like the Reviewers to Know:
	I am interested in being  a part of the data team. I would like to learn more about data
	collection and queries on network flow data. Thank you for considering my candidacy for this 
	position.
	